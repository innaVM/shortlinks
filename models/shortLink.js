"use strict";

var mongoose = require("libs/mongoose"),
    Schema = mongoose.Schema;

var schema = new Schema({
    userKey: {
        type: String,
        required: true
    },
    baseUrl: {
        type: String,
        required: true
    },
    shortUri: {
        type: String,
        unique: true,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    visits: {
        type: Number,
        default: 0,
        required: true
    }
});


exports.ShortLink = mongoose.model("ShortLink", schema);