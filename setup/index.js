"use strict";

var mongoose = require("libs/mongoose"),
    User = require("models/user").User;


function openConnection() {
    return new Promise(function (resolve, reject) {
        mongoose.connection.on("open", function (err) {
            if (err) {
                reject(err);
            }
            resolve("Database connected");
        });
    });
}

function dropDatabase() {
    return new Promise(function (resolve, reject) {
        mongoose.connection.db.dropDatabase(function (err) {
            if (err) {
                reject(err);
            }
            resolve("Database is dropped");

        });
    });

}
function createDefaultAdmin() {
    var admin = new User({
        username: "admin",
        email: "innaprogram@gmail.com",
        password: "password",
        role: "admin"
    });
    return admin.save();
}

function closeConnection() {
    return new Promise(function (resolve, reject) {
        mongoose.disconnect(function (err) {
            if (err) {
                reject(err);
            }
            resolve("Database closed");
        });
    });

}

openConnection()
    .then(function (result) {
        console.log(result);
        return dropDatabase();
    })
    .then(function (result) {
        console.log(result);
        return createDefaultAdmin();
    })
    .then(function (result) {
        console.log(result);
        return closeConnection();
    })
    .then(function (result) {
        console.log(result);
    })
    .catch(function (err) {
        closeConnection();
        console.log(err);
    });



