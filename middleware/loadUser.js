"use strict";

var User = require("models/user").User;

module.exports = function (req, res, next) {
    if (!req.session.userId) {
        return next();
    }
    User.findById(req.session.userId)
        .then(function (user) {
            if (!user) {
                throw new Error("User not found!");
            }
            req.user = res.locals.user = user;
            return next();


        })
        .catch(function (err) {
            return next(err);
        });
};