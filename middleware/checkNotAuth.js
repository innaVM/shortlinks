"use strict";

var httpError = require("http-error");

module.exports = function (req, res, next) {
    if (!req.session.userId) {
        return next(new httpError.Forbidden("Access is denied!"));
    }
    next();
};