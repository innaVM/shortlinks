"use strict";

var config = require("config");

module.exports = function (req, res, next) {
    res.locals.baseUrl = config.get("server:baseUrl") + ":" + config.get("server:port");
    next();
};