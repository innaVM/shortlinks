"use strict";

var mongoose = require('mongoose');
var config = require('config');

mongoose.Promise = require('bluebird');
mongoose.connect(
    config.get("mongoose:uri") + config.get("db:host") + "/" + config.get("db:name"),
    config.get("mongoose:options")
);

module.exports = mongoose;