var express = require('express');
var router = express.Router();

/* User log out */
router.post('/', function(req, res, next) {
    if (req.session) {
        req.session.destroy(function(){});
    }
    res.redirect('/');
});

module.exports = router;
