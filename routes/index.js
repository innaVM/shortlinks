"use strict";

var express = require('express');
var router = express.Router();
var ShortLink = require("models/shortLink").ShortLink;
var HttpError = require('http-error');

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Express'});
});

/* GET short link. */
router.get('/:short_link', function (req, res, next) {
    var shortLink = req.params.short_link;

    console.log(req.params);

    ShortLink.findOne({shortUri: shortLink})
        .then(function (link) {
            res.redirect(link.baseUrl);
            link.visits = link.visits + 1;
            console.log(link.visits);
            link.update({visits: link.visits}, function(){});
        })
        .catch(function (err) {
            return next(new HttpError.NotFound("Page not found!!!!"));
        });

});

module.exports = router;
