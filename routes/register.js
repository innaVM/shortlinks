"use strict";

var express = require('express');
var router = express.Router();
var User = require("models/user").User;

/* GET register page. */
router.get('/', function (req, res, next) {
    res.render('register', {title: 'Registration'});
});

/* POST  */
router.post('/', function (req, res, next) {
    var username = req.body.username || null;
    var email = req.body.email || null;
    var password = req.body.password || null;

    try {
        validateUserName(username);
        validateEmail(email);
        validatePassword(password);
    }
    catch (e) {
        e.status = 400;
        return next(e);
    }

    User.findOne({email: email})
        .then(function (user) {
            if(user){
                throw new Error("User with this email is exist!");
            }
            return createNewUser(username, email, password);
        })
        .then(function (newUser) {
            req.session.userId = newUser._id;
            res.redirect('/');
        })
        .catch(function (err) {
            console.log(err);
            return next(err);
        });

});


function validateUserName(userName) {
    var length = userName.length;

    if (!userName) {
        throw new Error("User name is required!");
    }

    if (length < 6) {
        throw new Error("The number of characters is less than 6!");
    }

    return true;
}

function validateEmail(email) {
    var pattern = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;

    if (!email) {
        throw new Error("Email is required!");
    }

    if (!pattern.test(email)) {
        throw new Error("Email is not correct!!");
    }

    return true;
}

function validatePassword(password) {
    var length = password.length;

    if (!password) {
        throw new Error("Password is required!");
    }

    if (length < 6) {
        throw new Error("The number of characters is less than 6!");
    }

    return true;
}

function createNewUser(username, email, password) {
    var user = new User({
        username: username,
        email: email,
        password: password
    });
    return user.save();
}

module.exports = router;
