"use strict";

var express = require('express');
var router = express.Router();
var User = require("models/user").User;
var checkAuth = require("middleware/checkAuth");

/* GET login page. */
router.get('/', checkAuth, function (req, res, next) {
    res.render('login', {title: 'Login'});
});

// Post from form
router.post('/', function (req, res, next) {
    var email = req.body.email || null;
    var password = req.body.password || null;

    User.findOne({email: email})
        .then(function (user) {
            if (!user) {
                throw new Error("Incorrect email");
            }
            if (!user.checkPassword(password)) {
                throw new Error("Incorrect password");
            }
            req.session.userId = user._id;
            res.redirect('/');
        })
        .catch(function (err) {
            return next(err);
        });

});

module.exports = router;
