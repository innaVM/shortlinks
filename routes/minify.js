"use strict";

var express = require('express');
var router = express.Router();
var checkNotAuth = require("middleware/checkNotAuth");
var ShortLink = require("models/shortLink").ShortLink;

var uid = require('rand-token').uid;
var config = require("config");
var HttpError = require('http-error');


function validateUrl(url) {
    var pattern = /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/i;

    if (!pattern.test(url)) {
        throw new Error("Url is not correct!!");
    }

    return true;
}

/* GET minify page. */
router.get('/', checkNotAuth, function (req, res, next) {
    var lastUri = req.session.lastGeneratedUri || null;
    if (lastUri) {
        req.session.lastGeneratedUri = null;
    }
    ShortLink.find({userKey: req.session.userId})
        .then(function (shortLinks) {
            res.render('minify', {shortUrl: lastUri, shortLinks: shortLinks});
        })
        .catch(function (err) {
            next(err);
        });
});

// Post
router.post('/', checkNotAuth, function (req, res, next) {
    var baseUrl = req.body.longUrl || null;
    var userKey = req.session.userId;

    try {
        validateUrl(baseUrl);
    }
    catch (e) {
        return next(new HttpError.BadRequest("Url is not valid!"));
    }
    console.log("UserKey: " + userKey);

    var newlink = new ShortLink({
        userKey: userKey,
        baseUrl: baseUrl,
        shortUri: uid(config.get("minifyOptions:length"))
    });

    newlink.save()
        .then(function (newShortLink) {
            req.session.lastGeneratedUri = newShortLink.shortUri;
            res.redirect('/minify');
        })
        .catch(function (err) {
            return next(err);
        });

});


module.exports = router;
